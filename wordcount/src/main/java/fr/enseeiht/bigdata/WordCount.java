package fr.enseeiht.bigdata;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;
import java.util.Arrays;
import java.util.Iterator;

/**
 * @author muquanrui
 * @date 21/01/2022 20:19
 */
public class WordCount {

        public static void main(String[] args) {
            //创建SparkConf,并指定应用程序名称
            SparkConf conf = new SparkConf().setAppName("JavaWordCount");

            //创建JavaSparkContext
            JavaSparkContext sc = new JavaSparkContext(conf);

            //从文件系统读取数据
            //注意在Java的数组取下标使用中括号args[0],而scala使用小括号args(0)
            //其实JavaRDD继承了Spark的RDD,对其做了扩展
            JavaRDD<String> lines = sc.textFile(args[0]);

            JavaRDD<String> words = lines.flatMap(new FlatMapFunction<String, String>() {
                @Override
                public Iterator<String> call(String line) throws Exception {
                    return Arrays.asList(line.split(" ")).iterator();
                }
            });

            JavaPairRDD<String, Integer> ones = words.mapToPair(new PairFunction<String, String, Integer>() {
                @Override
                public Tuple2<String, Integer> call(String word) throws Exception {
                    return new Tuple2<String, Integer>(word, 1);
                }
            });

            JavaPairRDD<String, Integer> counts =  ones.reduceByKey(new Function2<Integer, Integer, Integer>() {
                @Override
                public Integer call(Integer i1, Integer i2) throws Exception {
                    return i1 + i2;
                }
            });

            JavaPairRDD<Integer, String> swapedPair = counts.mapToPair(new PairFunction<Tuple2<String, Integer>, Integer, String>() {
                @Override
                public Tuple2<Integer, String> call(Tuple2<String, Integer> tp) throws Exception {
                    //将元组中数据交换位置的第一种方式(下面还有第二种方式)
                    return new Tuple2<Integer, String>(tp._2, tp._1);
                }
            });

            JavaPairRDD<Integer, String> sortedPair = swapedPair.sortByKey(false);

            JavaPairRDD<String, Integer> finalResult = sortedPair.mapToPair(new PairFunction<Tuple2<Integer, String>, String, Integer>() {
                @Override
                public Tuple2<String, Integer> call(Tuple2<Integer, String> tp) throws Exception {
                    //将元组中的数据交换位置的第二种方式(swap就是交换的意思)
                    return tp.swap();
                }
            });

            //将结果存储到文件系统
            finalResult.saveAsTextFile(args[1]);

            //释放资源
            sc.stop();
        }
}
