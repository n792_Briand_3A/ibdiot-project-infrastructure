#### Folder tree

in /ansible can be found the script to launch and install spark/hadoop

in bigdata-project, the website

in wordcount, the bigdata application

#### quick start

`git clone https://gitlab.com/n792_Briand_3A/ibdiot-project-infrastructure.git`

#### Install ansible on controller

`sudo apt install ansible`

#### Dans IPinv, lister les nodes qu'on va appeler dans le playbook

```
[slave]
<IP address of slave>
<IP address of slave2>
[master]
<IP address of master>
```

#### launch

Aller dans le répertoire et lancer (vérifier les variables au début du script avant)

```
cd ansible/hadoop

ansible -K main.yml
```



#### VM setup avant de pouvoir être tranquille

On fait un dhclient pour récup ip des vms
`sudo dhclient`

Pour chaque VM :

`sudo echo master > /etc/hostname`
`sudo echo slave1 > /etc/hostname`
`sudo echo slave2 > /etc/hostname`

ou alors dans /etc/rc.local
```
#!/bin/bash
dhclient
exit0
```
activer le service

```
systemctl enable rc-local
systemctl restart rc-local
systemctl status rc-local
```

`source /etc/profile`

#### installer python sur les vms

`sudo apt update python`

#### à faire a la main pour enlever le ssh (voir comment on peut fix l'automatisation

```
ssh-copy-id -i /root/.ssh/id_rsa.pub master
ssh-copy-id -i /root/.ssh/id_rsa.pub slave1
ssh-copy-id -i /root/.ssh/id_rsa.pub slave2
```

ou lancer sur le master bigdata/pubkeys.sh




#### Generate hash of password needed for root password
ansible all -i localhost, -m debug -a "msg={{ '980430' | password_hash('sha512', '980430') }}"
$6$980430$Ww2/wGADaeXInTo0PwWt4G.0ehIYorc4VCGAfg1i5bcs0ezr1SgRmjreY9c5dz/1rNL9Rqi25X8Tpd9OLlxU21
