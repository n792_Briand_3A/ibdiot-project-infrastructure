package fr.enseeiht.project.controller;

import fr.enseeiht.project.service.RunProgramService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author muquanrui
 * @date 11/01/2022 17:11
 */

@Controller
public class ViewController {
    @RequestMapping("/upload")
    public String testUp(MultipartFile program, MultipartFile datafile, String mainMethod, HttpSession session, Model model) throws IOException {
        ServletContext servletContext = session.getServletContext();

        String programName = program.getOriginalFilename();
        String programsPath = servletContext.getRealPath("programs");
        File programFile = new File(programsPath);
        if (!programFile.exists()) {
            programFile.mkdir();
        }
        String programFinalPath = programsPath + File.separator + programName;
        program.transferTo(new File(programFinalPath));

        String datafileName = datafile.getOriginalFilename();
        String filePath = servletContext.getRealPath("files");
        File dataFile = new File(filePath);
        if (!dataFile.exists()) {
            dataFile.mkdir();
        }
        String dataFinalPath = filePath + File.separator + datafileName;
        datafile.transferTo(new File(dataFinalPath));

        model.addAttribute("programName", programName);
        model.addAttribute("programFinalPath", programFinalPath);
        model.addAttribute("outputPath", servletContext.getRealPath("outputs"));
        model.addAttribute("datafileName", datafileName);
        model.addAttribute("dataFinalPath", dataFinalPath);
        model.addAttribute("mainMethod", mainMethod);
        return "afterUpload";
    }

    @RequestMapping("/readytodownload")
    public String download(String programName, String programFinalPath, String outputPath,
                           String datafileName, String dataFinalPath, String mainMethod,
                           Model model) {
        RunProgramService runProgramService = new RunProgramService();
        runProgramService.setProgram(programName, programFinalPath, outputPath, datafileName, dataFinalPath, mainMethod);
        new Thread(runProgramService).start();
        model.addAttribute("outputName", programName + "_output" + ".txt");
        model.addAttribute("isOutputed", "false");
        // model.addAttribute("outputPath", runProgram.getOutputPath());
        return "download";
    }

    @RequestMapping("/testDown")
    public String testDownload(HttpSession session, String outputName, Model model) {
        ServletContext servletContext = session.getServletContext();
        String realPath = servletContext.getRealPath("/outputs" + File.separator + outputName);
        File outputFile = new File(realPath);
        if (outputFile.exists()) {
            model.addAttribute("isOutputed", "true");
            model.addAttribute("outputName", outputName);
            return "forward:/canDownload";
        } else {
            model.addAttribute("isOutputed", "false");
            model.addAttribute("outputName", outputName);
            return "download";
        }
    }

    @RequestMapping("/canDownload")
    public ResponseEntity<byte[]> testResponseEntity(HttpSession session, String outputName) throws IOException {
        //获取ServletContext对象
        ServletContext servletContext = session.getServletContext();
        //获取服务器中文件的真实路径
        String realPath = servletContext.getRealPath("/outputs" + File.separator + outputName);
        //创建输入流
        InputStream is = new FileInputStream(realPath);
        //创建字节数组
        byte[] bytes = new byte[is.available()];
        is.read(bytes);
        //创建HttpHeaders对象设置响应头信息
        MultiValueMap<String, String> headers = new HttpHeaders();
        //设置要下载方式以及下载文件的名字
        headers.add("Content-Disposition", "attachment;filename=" + outputName);
        //设置响应状态码
        HttpStatus statusCode = HttpStatus.OK;
        //创建ResponseEntity对象
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(bytes, headers, statusCode);
        //关闭输入流
        is.close();
        return responseEntity;
    }
}
