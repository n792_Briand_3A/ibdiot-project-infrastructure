package fr.enseeiht.project.service;

import fr.enseeiht.project.bean.ProgramBean;
import org.springframework.stereotype.Service;

import java.io.*;

/**
 * @author muquanrui
 * @date 22/01/2022 15:42
 */
@Service
public class RunProgramService implements Runnable {
    private ProgramBean programBean;

    public RunProgramService() {
        programBean = new ProgramBean();
    }

    public void setProgram(String programName, String programPath, String outputPath,
                           String datafileName, String datafilePath, String mainMethod) {
        programBean.setProgramName(programName);
        programBean.setProgramPath(programPath);
        programBean.setOutputPath(outputPath);
        programBean.setDatafileName(datafileName);
        programBean.setDatafilePath(datafilePath);
        programBean.setMainMethod(mainMethod);
    }

    public String getOutputPath() {
        return programBean.getOutputPath();
    }

    @Override
    public void run() {
        InputStreamReader ir = null;
        LineNumberReader input = null;
        FileWriter fileWriter = null;

        try {

            Process process = Runtime.getRuntime().exec("hdfs dfs -rm /input/" + programBean.getDatafileName());  //调用Linux的相关命令
            process.waitFor();

            process = Runtime.getRuntime().exec("hdfs dfs -put "+ programBean.getDatafilePath() + " /input");
            process.waitFor();

            process = Runtime.getRuntime().exec("hdfs dfs -rm -r /output");
            process.waitFor();

            process = Runtime.getRuntime().exec("spark-submit " +
                    "--master spark://192.168.91.100:7077 " +
                    "--executor-memory 512m " +
                    "--total-executor-cores 4 " +
                    "--class "+ programBean.getMainMethod() + " " +
                    programBean.getProgramPath() + " " +
                    "hdfs://192.168.91.100:9000/input " +
                    "hdfs://192.168.91.100:9000/output");
            process.waitFor();

            File outputDir = new File(programBean.getOutputPath());
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }

            File outputFile = new File(outputDir + File.separator + programBean.getProgramName() + "_output" + ".txt");
            if (outputFile.exists()) {
                outputFile.delete();
            }
            // outputFile.createNewFile();

            Runtime.getRuntime().exec("hadoop fs -getmerge /output " + outputFile);
//            fileWriter = new FileWriter(outputFile, true);
//
//            ir = new InputStreamReader(process.getInputStream());
//            input = new LineNumberReader(ir);      //创建IO管道，准备输出命令执行后的显示内容
//
//            String line;
//            fileWriter.append("Here is the result after running the program:\n");
//            while ((line = input.readLine()) != null) {//按行打印输出内容
//                System.out.println(line);
//                fileWriter.append("\n" + line) ;
//            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                ir.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
