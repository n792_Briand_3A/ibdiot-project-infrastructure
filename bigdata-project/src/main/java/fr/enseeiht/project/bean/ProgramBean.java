package fr.enseeiht.project.bean;

import org.springframework.stereotype.Component;

/**
 * @author muquanrui
 * @date 22/01/2022 15:40
 */

public class ProgramBean {
    private String programName;
    private String programPath;
    private String outputPath;
    private String datafileName;
    private String datafilePath;
    private String mainMethod;

    public String getProgramName() {
        return programName;
    }

    public String getProgramPath() {
        return programPath;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public String getDatafileName() {
        return datafileName;
    }

    public String getDatafilePath() {
        return datafilePath;
    }

    public String getMainMethod() {
        return mainMethod;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public void setProgramPath(String programPath) {
        this.programPath = programPath;
    }

    public void setOutputPath(String outputPath) {
        this.outputPath = outputPath;
    }

    public void setDatafileName(String datafileName) {
        this.datafileName = datafileName;
    }

    public void setDatafilePath(String datafilePath) {
        this.datafilePath = datafilePath;
    }

    public void setMainMethod(String mainMethod) {
        this.mainMethod = mainMethod;
    }
}
