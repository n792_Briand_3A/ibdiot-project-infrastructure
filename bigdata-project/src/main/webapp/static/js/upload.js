function validateForm() {
    var program = document.getElementById("program");
    var datafile = document.getElementById("datafile");
    if (program.files[0] == undefined || datafile.files[0] == undefined) {
        alert("You have to upload both program and data file!");
        return false;
    }
    var programName = program.files[0].name;
    var datafileName = datafile.files[0].name;
    var programSuffix = programName.substring(programName.lastIndexOf("."));
    var datafileSuffix = datafileName.substring(datafileName.lastIndexOf("."));
    if (programSuffix != ".jar" || datafileSuffix != ".txt") {
        alert("Please make sure that your program is \".jar\" and your data file is \".txt\".");
        return false;
    }
    var mainMethod = document.getElementById("mainMethod").value;
    var pattern = new RegExp("([a-zA-Z_][a-zA-Z0-9_]*[.])*([a-zA-Z_][a-zA-Z0-9_]*)$");
    if (!pattern.test(mainMethod)) {
        alert("Please enter the correct main method for your JAR!");
        return false;
    }

    return true;
}